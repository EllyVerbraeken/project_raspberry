-- Insert Stored Procedure for Warning
USE elly;
DROP PROCEDURE IF EXISTS `WarningInsert`;
DELIMITER //
CREATE PROCEDURE `WarningInsert`
(
	OUT pId INT ,
	IN pClient VARCHAR (80) CHARACTER SET UTF8 ,
    IN pTemperature FLOAT,
 	IN pLevel INT,
	IN pTime TIMESTAMP
)
BEGIN
INSERT INTO `Warning`
	(
		`Warning`.`Client`,
		`Warning`.`Temperature`,
		`Warning`.`Level`,
        `Warning`.`Time`
	)
	VALUES
	(
		pClient,
		pTemperature,
		pLevel,
		pTime
    );
	SELECT LAST_INSERT_ID() INTO pId;
END //
DELIMITER ;

-- Delete by Day Stored Procedure for Warning
USE elly;
DROP PROCEDURE IF EXISTS `WarningDeleteByDay`;
DELIMITER //
CREATE PROCEDURE `WarningDeleteByDay`
(
	IN pTime TIMESTAMP 
)
BEGIN
delete from Warning
where `Warning`.`Time` < pTime;
END //
DELIMITER ;

-- Delete by Client Stored Procedure for Warning
USE elly;
DROP PROCEDURE IF EXISTS `WarningDeleteByClient`;
DELIMITER //
CREATE PROCEDURE `WarningDeleteByClient`
(
	IN pClient VARCHAR (80) CHARACTER SET UTF8 
)
BEGIN
delete from Warning
where `Warning`.`Client` = pClient;
END //
DELIMITER ;

-- Select Warning by Client Stored Procedure for Warning
USE elly;
DROP PROCEDURE IF EXISTS `WarningSelect`;
DELIMITER //
CREATE PROCEDURE `WarningSelect`
(
	IN pClient VARCHAR (80) CHARACTER SET UTF8 
)
BEGIN
select `Warning`.`Level`, `Warning`.`Temperature`, `Warning`.`Time` from Warning
where `Warning`.`Client` = pClient;
END //
DELIMITER ;