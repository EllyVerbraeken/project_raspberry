USE `elly`;
DROP TABLE IF EXISTS `CalculatedTemperatures`;
CREATE TABLE `CalculatedTemperatures` (
	`Id` INT NOT NULL AUTO_INCREMENT,
	CONSTRAINT PRIMARY KEY(Id),
	`Client` VARCHAR (80) CHARACTER SET UTF8 NOT NULL,
		`Minimum` FLOAT NOT NULL,
	`Maximum` FLOAT NOT NULL,
	`Average` FLOAT NOT NULL,
	`Day` TIMESTAMP NOT NULL);