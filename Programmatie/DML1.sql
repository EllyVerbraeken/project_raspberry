-- Delete by Client Stored Procedure for TemperatureRaspberry
USE elly;
DROP PROCEDURE IF EXISTS `TemperatureRaspberryDelete`;
DELIMITER //
CREATE PROCEDURE `TemperatureRaspberryDelete`
(
	IN pClient VARCHAR (80) CHARACTER SET UTF8 
)
BEGIN
delete from TemperatureRaspberry
where `TemperatureRaspberry`.`Client` = pClient;
END //
DELIMITER ;

-- Select by Client Stored Procedure for TemperatureRaspberry
USE elly;
DROP PROCEDURE IF EXISTS `TemperatureRaspberrySelect`;
DELIMITER //
CREATE PROCEDURE `TemperatureRaspberrySelect`
(
	IN pClient VARCHAR (80) CHARACTER SET UTF8 
)
BEGIN
select `TemperatureRaspberry`.`Degree` from TemperatureRaspberry
where `TemperatureRaspberry`.`Client` = pClient;
END //
DELIMITER ;