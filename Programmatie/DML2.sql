-- Insert Stored Procedure for CalculatedTemperatures
USE elly;
DROP PROCEDURE IF EXISTS `CalculatedTemperaturesInsert`;
DELIMITER //
CREATE PROCEDURE `CalculatedTemperaturesInsert`
(
	OUT pId INT ,
	IN pClient VARCHAR (80) CHARACTER SET UTF8 ,
    IN pMinimum FLOAT,
 	IN pMaximum FLOAT,
    IN pAverage FLOAT,
	IN pDay TIMESTAMP
)
BEGIN
INSERT INTO `CalculatedTemperatures`
	(
		`CalculatedTemperatures`.`Client`,
		`CalculatedTemperatures`.`Minimum`,
		`CalculatedTemperatures`.`Maximum`,
		`CalculatedTemperatures`.`Average`,
        `CalculatedTemperatures`.`Day`
	)
	VALUES
	(
		pClient,
		pMinimum,
		pmaximum,
        pAverage,
		pDay
    );
	SELECT LAST_INSERT_ID() INTO pId;
END //
DELIMITER ;

-- Delete by Day Stored Procedure for CalculatedTemperatures
USE elly;
DROP PROCEDURE IF EXISTS `CalculatedTemperaturesDelete`;
DELIMITER //
CREATE PROCEDURE `CalculatedTemperaturesDelete`
(
	IN pDay TIMESTAMP 
)
BEGIN
delete from `CalculatedTemperatures`
where `CalculatedTemperatures`.`Day` < pDay;
END //
DELIMITER ;

-- Select Average by Client Stored Procedure for CalculatedTemperatures
USE elly;
DROP PROCEDURE IF EXISTS `CalculatedTemperaturesSelectAverage`;
DELIMITER //
CREATE PROCEDURE `CalculatedTemperaturesySelectAverage`
(
	IN pClient VARCHAR (80) CHARACTER SET UTF8 
)
BEGIN
select `CalculatedTemperatures`.`Average` from CalculatedTemperatures
where `CalculatedTemperatures`.`Client` = pClient;
END //
DELIMITER ;

-- Select Minimum by Client Stored Procedure for CalculatedTemperatures
USE elly;
DROP PROCEDURE IF EXISTS `CalculatedTemperaturesSelectMinimum`;
DELIMITER //
CREATE PROCEDURE `CalculatedTemperaturesySelectMinimum`
(
	IN pClient VARCHAR (80) CHARACTER SET UTF8 
)
BEGIN
select `CalculatedTemperatures`.`Minimum` from CalculatedTemperatures
where `CalculatedTemperatures`.`Client` = pClient;
END //
DELIMITER ;

-- Select Maximum by Client Stored Procedure for CalculatedTemperatures
USE elly;
DROP PROCEDURE IF EXISTS `CalculatedTemperaturesSelectMaximum`;
DELIMITER //
CREATE PROCEDURE `CalculatedTemperaturesySelectMaximum`
(
	IN pClient VARCHAR (80) CHARACTER SET UTF8 
)
BEGIN
select `CalculatedTemperatures`.`Maximum` from CalculatedTemperatures
where `CalculatedTemperatures`.`Client` = pClient;
END //
DELIMITER ;
