USE elly;
DROP PROCEDURE IF EXISTS `TemperatureRaspberryInsert`;
DELIMITER //
CREATE PROCEDURE `TemperatureRaspberryInsert`
(
	OUT pId INT ,
	IN pClient VARCHAR (80) CHARACTER SET UTF8 ,
    IN pDegree FLOAT,
	IN pWarning VARCHAR (10) CHARACTER SET UTF8, 	
	IN pTime TIMESTAMP
)
BEGIN
INSERT INTO `TemperatureRaspberry`
	(
		`TemperatureRaspberry`.`Client`,
		`TemperatureRaspberry`.`Degree`,
		`TemperatureRaspberry`.`Warning`,
		`TemperatureRaspberry`.`Time`
	)
	VALUES
	(
		pClient,
		pDegree,
		pWarning,
		pTime
    );
	SELECT LAST_INSERT_ID() INTO pId;
END //
DELIMITER ;